const matches = require('../public/output/matches.json')
const fs = require('fs')

// Function to find a player who has won the highest number of Player of the Match awards for each season
const matchAwardsEachSeason = matchesData => {
  const playerCounts = {}

  for (let i = 0; i < matchesData.length; i++) {
    const element = matchesData[i]
    const season = element.season
    const player_of_match = element.player_of_match

    if (!playerCounts[season]) {
      playerCounts[season] = {}
    }

    if (playerCounts[season]) {
      if (playerCounts[season][player_of_match]) {
        playerCounts[season][player_of_match] += 1
      } else {
        playerCounts[season][player_of_match] = 1
      }
    }
  }

  const playerOfTheMatchCounts = playerCounts
  const highestPlayersPerSeason = {}

  const seasons = Object.keys(playerOfTheMatchCounts)
  for (let j = 0; j < seasons.length; j++) {
    const season = seasons[j]
    const playersCount = playerOfTheMatchCounts[season]

    let highestPlayer = null
    let highestCount = 0

    // Loop through playersCount to find the highest player
    const players = Object.keys(playersCount)
    for (let k = 0; k < players.length; k++) {
      const player = players[k]
      const count = playersCount[player]

      if (count > highestCount) {
        highestPlayer = player
        highestCount = count
      }
    }

    highestPlayersPerSeason[season] = {
      player: highestPlayer,
      count: highestCount
    }
  }

  return highestPlayersPerSeason
}

const res = matchAwardsEachSeason(matches)

fs.writeFileSync(`../public/output/PlayerOfTheMatches.json`,
  JSON.stringify(res, null, 2)
)
