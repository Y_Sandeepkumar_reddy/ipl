const matchesData = require('../public/output/matches.json');
const deliveriesData = require('../public/output/deliveries.json');
const fs = require('fs');


const topEconomicalBowlers = (matchesData, deliveriesData, year, top) => {
  const matchesInYear = [];
  for (const element of matchesData) {
    if (element.season === year) {
      matchesInYear.push(element);
    }
  }

  const matchIdsInYear = matchesInYear.map(match => match.id);

  const bowlersData = [];
  for (const bowler of deliveriesData) {
    if (matchIdsInYear.includes(bowler.match_id)) {
      bowlersData.push(bowler);
    }
  }

  const RunsBalls = {};
  for (const delivery of bowlersData) {
    const bowler = delivery.bowler;
    const runs = Number(delivery.total_runs) - Number(delivery.bye_runs);
    const balls =
      delivery.noball_runs === '0' && delivery.wide_runs === '0' ? 1 : 0;
    if (RunsBalls[bowler]) {
      RunsBalls[bowler].runs += runs;
      RunsBalls[bowler].balls += balls;
    } else {
      RunsBalls[bowler] = { runs, balls };
    }
  }

  const bowlerEconomy = [];
  for (const bowler in RunsBalls) {
    const { runs, balls } = RunsBalls[bowler];
    const economy = (runs / balls) * 6;
    bowlerEconomy.push({ bowler, economy });
  }

  bowlerEconomy.sort((a, b) => a.economy - b.economy);

  const EconomicalBowlers = bowlerEconomy.slice(0, top);

  return EconomicalBowlers;
};


const top10EconomicalBowlers = topEconomicalBowlers(matchesData, deliveriesData, '2015', 10);


fs.writeFileSync(`../public/output/topEconamiBowlers_2015.json`, JSON.stringify(top10EconomicalBowlers, null, 2));
