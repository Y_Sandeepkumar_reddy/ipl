const deliveriesData = require('../public/output/deliveries.json');
const fs = require('fs');
const path = require('path');

const bestEconomyInSuperOvers = () => {
    const economyMap = {};

    for (const delivery of deliveriesData) {
        const bowler = delivery.bowler;
        const isSuperOver = delivery.is_super_over === '1';
        const runs = parseInt(delivery.total_runs);
        const extras = parseInt(delivery.extra_runs);

        if (isSuperOver) {
            economyMap[bowler] = economyMap[bowler] || { runs: 0, balls: 0 };

            economyMap[bowler].runs += runs;
            economyMap[bowler].balls += 1;

            if (delivery.wide_runs === '0' && delivery.noball_runs === '0') {
                economyMap[bowler].balls -= extras;
            }
        }
    }

    // Calculate economy for each bowler
    const bowlers = Object.keys(economyMap).map(bowler => {
        const { runs, balls } = economyMap[bowler];
        const economy = (runs / balls) * 6;
        return { bowler, economy };
    });

    bowlers.sort((a, b) => a.economy - b.economy);

    const bestEconomyBowler = bowlers[0];

    const outputPath = path.join(__dirname, '../public/output/bestEconomyInSuperOvers.json');
    fs.writeFileSync(outputPath, JSON.stringify(bestEconomyBowler, null, 2));
};

bestEconomyInSuperOvers();
