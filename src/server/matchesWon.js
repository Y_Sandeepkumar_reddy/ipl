const matchesData = require('./../public/output/matches.json');
const fs = require('fs');
const path = require('path');

const matchesWonPerTeamPerYear = () => {
    const matchesWonPerTeamPerYearResult = matchesData.reduce((acc, match) => {
        const year = match.season;
        const winner = match.winner;

        if (!acc[year]) {
            acc[year] = {};
        }

        
        if (winner && /^[a-zA-Z\s]+$/.test(winner)) {
            acc[year][winner] = (acc[year][winner] || 0) + 1;
        }

        return acc;
    }, {});

    const outputPath = path.join(__dirname, '../public/output/matchesWonPerTeamPerYear.json');
    fs.writeFileSync(outputPath, JSON.stringify(matchesWonPerTeamPerYearResult, null, 2));
};

matchesWonPerTeamPerYear();
