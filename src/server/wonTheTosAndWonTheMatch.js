const matchesData = require('../public/output/matches.json');
const fs = require('fs');
const path = require('path');

const tossAndMatchWins = () => {
    const tossAndMatchWinsMap = {};

    for (const match of matchesData) {
        const tossWinner = match.toss_winner;
        const matchWinner = match.winner;

        if (tossWinner && matchWinner && tossWinner === matchWinner) {
            tossAndMatchWinsMap[tossWinner] = (tossAndMatchWinsMap[tossWinner] || 0) + 1;
        }
    }

    const outputPath = path.join(__dirname, '../public/output/tossAndMatchWins.json');
    fs.writeFileSync(outputPath, JSON.stringify(tossAndMatchWinsMap, null, 2));
};

tossAndMatchWins();
