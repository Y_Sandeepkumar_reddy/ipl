const deliveriesData = require('../public/output/deliveries.json');
const fs = require('fs');
const path = require('path');

const batsmanStrikeRatePerSeason = () => {
    const strikeRateMap = {};

    for (const delivery of deliveriesData) {
        const batsman = delivery.batsman;
        const season = delivery.match_id.substring(0, 4); 
        const runs = parseInt(delivery.batsman_runs);
        const isWideOrNoBall = delivery.wide_runs !== '0' || delivery.noball_runs !== '0';

        if (batsman && season && !isWideOrNoBall) {
            strikeRateMap[season] = strikeRateMap[season] || {};
            strikeRateMap[season][batsman] = strikeRateMap[season][batsman] || { runs: 0, balls: 0 };

            strikeRateMap[season][batsman].runs += runs;
            strikeRateMap[season][batsman].balls += 1;
        }
    }

    const strikeRatePerSeason = {};

    // Calculate strike rate for each batsman in each season
    for (const season in strikeRateMap) {
        strikeRatePerSeason[season] = {};

        for (const batsman in strikeRateMap[season]) {
            const { runs, balls } = strikeRateMap[season][batsman];
            const strikeRate = (runs / balls) * 100;
            strikeRatePerSeason[season][batsman] = strikeRate.toFixed(2);
        }
    }

    const outputPath = path.join(__dirname, '../public/output/batsmanStrikeRatePerSeason.json');
    fs.writeFileSync(outputPath, JSON.stringify(strikeRatePerSeason, null, 2));
};

batsmanStrikeRatePerSeason();
