

const matchesData = require('./../public/output/matches.json');
const fs = require('fs');
const path = require('path');

const matchesPerYear = matchesData.reduce((acc, match) => {
    const year = match.season;

    if (!acc[year]) {
        acc[year] = 1;
    } else {
        acc[year]++;
    }
    return acc;
}, {});

const outputPath = path.join( '../public/output/matchesPerYear.json');

fs.writeFileSync(outputPath, JSON.stringify(matchesPerYear, null, 2));
