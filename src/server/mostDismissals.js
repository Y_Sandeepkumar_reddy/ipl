//8. Find the highest number of times one player has been dismissed by another player
const deliveriesData = require('./../public/output/deliveries.json')
const fs = require('fs')



const dismissalCount = {}

for (let i = 0; i < deliveriesData.length; i++) {
  const delivery = deliveriesData[i]
  const dismissedPlayer = delivery.player_dismissed
  const dismissalKind = delivery.dismissal_kind

  if (dismissedPlayer && dismissalKind !== 'run out') {
    if (!dismissalCount[dismissedPlayer]) {
      dismissalCount[dismissedPlayer] = {}
    }
    if (!dismissalCount[dismissedPlayer][delivery.bowler]) {
      dismissalCount[dismissedPlayer][delivery.bowler] = 1
    } else {
      dismissalCount[dismissedPlayer][delivery.bowler]++
    }
  }
}

const highestDismissalCount = {}

const dismissedPlayers = Object.keys(dismissalCount)
for (let j = 0; j < dismissedPlayers.length; j++) {
  const dismissedPlayer = dismissedPlayers[j]
  const bowlers = dismissalCount[dismissedPlayer]

  let highestBowler = null
  let highestCount = 0

  const bowlerNames = Object.keys(bowlers)
  for (let k = 0; k < bowlerNames.length; k++) {
    const bowler = bowlerNames[k]
    const count = bowlers[bowler]

    if (count > highestCount) {
      highestBowler = bowler
      highestCount = count
    }
  }

  highestDismissalCount[dismissedPlayer] = {
    bowler: highestBowler,
    count: highestCount
  }
}

fs.writeFileSync(
  '../public/output/highestDismissals.json',
  JSON.stringify(highestDismissalCount, null, 2)
)
